# {PKG_NAME}.jl
[![pipeline status]({PKG_URL}/badges/master/build.svg)]({PKG_URL}/pipelines)
[![coverage report]({PKG_URL}/badges/master/coverage.svg)]({PKG_CODECOV_URL}/branch/master)
[![documentation](https://img.shields.io/badge/docs-latest-blue.svg)]({PKG_DOC_URL})
