include("init.jl")
# HACK: https://github.com/JuliaImages/ImageView.jl/pull/156
# this is necessary to get ImageMagick to play nicely
# import ImageMagick
Pkg.add("Documenter")
Pkg.update()
include("../docs/make.jl")