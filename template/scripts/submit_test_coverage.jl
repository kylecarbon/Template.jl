
include("init.jl")

cd(Pkg.dir("{PKG_NAME}"))

Pkg.add("Coverage")
using Coverage
Codecov.submit_local(process_folder())
