#!/usr/bin/env julia
using Pkg

# Set package directory as primary environment for package manager
cur_dir = @__DIR__
Pkg.activate(abspath(joinpath(cur_dir, "../")))
Pkg.instantiate()
