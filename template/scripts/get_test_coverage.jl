include("init.jl")

Pkg.test("{PKG_NAME}"; coverage=true)

Pkg.add("Coverage")
Pkg.update()

cd(Pkg.dir("{PKG_NAME}"))

using Coverage, {PKG_NAME}
coverage = process_folder()
covered_lines, total_lines = get_summary(coverage)
percentage = covered_lines / total_lines * 100.0
println("($(percentage)%) covered")
if isinteractive()
    clean_folder({PKG_NAME}.dir())
end
