# {PKG_NAME}

The package repository is <{PKG_URL}>.

## Quickstart
```
using Pkg
Pkg.add("{PKG_URL}")
using {PKG_NAME}
```
