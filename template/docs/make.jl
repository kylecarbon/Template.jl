using Documenter, {PKG_NAME}

makedocs(format = Documenter.HTML(),
         sitename = "{PKG_NAME}.jl",
         pages = ["Home" => "index.md",
                  "Tutorials" => ["tutorials/tutorial0.md",
                                  "tutorials/tutorial1.md"],
									"API" => ["pages/section0.md",
                            "pages/section1.md"],
                  ],
         repo = "{PKG_URL}/blob/{commit}{path}#{line}"
         )