using Literate

OUTPUT = joinpath(@__DIR__, "generated/")

example0 = joinpath(@__DIR__, "example0.jl")
Literate.markdown(example0, OUTPUT; credit = false, documenter = true)
Literate.notebook(example0, OUTPUT; credit = false)