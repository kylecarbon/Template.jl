#!/usr/bin/env julia

using Pkg, Dates

###############################################################################
# Set up
###############################################################################

# TODO: switch to using ArgParse.jl
include(joinpath(@__DIR__, "config.jl"))

welcome_str = "Welcome to Template.jl -- using Julia version: $(VERSION)."
@info welcome_str

minver = v"1.0.0"
err(msg) = (printstyled(stderr, msg * '\n'; bold = true, color = :blue); exit(1))
VERSION ≥ minver || err("need at least version $(minver)")

1 == length(ARGS) || err("Usage: [julia] Template.jl destination_directory")

destdir = ARGS[1]

###############################################################################
# Helper methods
###############################################################################

"""
    replace_occurrences(str, replacements)

Use `replacements` to replace occurrences in `str`, using `replace`.

1. `str`: original string to modify
1. `replacement`: a dictionary, in which keys will be replaced by values
"""
function replace_multiple(str, replacements)
    for pair in replacements
        str = replace(str, pair)
    end
    str
end

"""
    copy_and_substitute(srcdir, destdir, replacements)

Copy from `srcdir` to `destdir` recursively, making the substitutions of contents and
filenames using `replacements`.

1. `srcdir`: the source directory of contents to be copied
1. `destdir`: the destination directory to where contents will be copied
1. `replacement`: a dictionary, in which keys will be replaced by values
"""
function copy_and_substitute(srcdir, destdir, replacements)
    for (root, dirs, files) in walkdir(srcdir)
        relroot = relpath(root, srcdir)
        mkpath(normpath(destdir, relroot))
        for file in files
            srcfile = joinpath(root, file)
            destfile = normpath(joinpath(destdir, relroot,
                                         replace_multiple(file, replacements)))
            if isfile(destfile)
                println("$(destfile) exists, skipping")
            else
                println("$(srcfile)  =>  $(destfile)")
                srcstring = read(srcfile, String)
                deststring = replace_multiple(srcstring, replacements)
                write(destfile, deststring)
            end
        end
    end
end

###############################################################################
# Run script
###############################################################################
if isdir(destdir)
    @info "Destination directory already exists -- exiting."
    exit(0)
end

srcdir = joinpath(@__DIR__, "template")
info_str = "Copying contents...\n\tsource: $srcdir\n\tdestination: $destdir"
@info info_str
copy_and_substitute(srcdir, destdir, template_config)