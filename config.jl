# TODO: switch to using ArgParse.jl

pkgname = "PkgName"
template_config = [
    "{PKG_NAME}" => pkgname,
    "{PKG_UUID}" => Pkg.METADATA_compatible_uuid(pkgname),
    "{PKG_AUTHOR_NAME}" => "Kyle Carbon",
    "{PKG_AUTHOR_EMAIL}" => "kylecarbon@gmail.com",
    "{PKG_URL}" => "https://gitlab.com/kylecarbon/$(pkgname).jl",
    "{PKG_DOC_URL}" => "{PKG_DOC_URL}",
    "{PKG_CODECOV_URL}" => "{PKG_CODECOV_URL}",
    "{PKG_YEAR}" => Dates.year(Dates.today())
]