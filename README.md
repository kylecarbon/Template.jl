# Template.jl
Inspired by [skeleton.jl](https://github.com/tpapp/skeleton.jl).

## Installation
This is not a package -- it is a simple script.
```
git clone https://gitlab.com/kylecarbon/Template.jl
```

## Usage
### Configuration
Update `config.jl` with your new package settings, e.g. `PKG_NAME`, `PKG_AUTHOR`, etc.

### Run
From your terminal, run
```
julia path/to/Template.jl destination/directory
```